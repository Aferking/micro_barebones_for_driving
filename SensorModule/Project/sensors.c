/*
Collection of functions for converting raw data. Either scaling, linearizing or both.
*/
#include "stdint.h"
#include "math.h"

#include "BSP_ADC_SIMPLE.h"
#include "BSP_CAN.h"
#include "CAN_DEFINES.h"
#include "globals.h"

/* Function prototypes */
static float scaleInput(AIN_typeDef input);

/* Constants */
#define TRHOTTLE_IMPLAUSABILITY_THRESHOLD 0.1f

/**

 @brief Tests for implausability, invalid values and scales the sensor value.
*/
uint16_t sensors_processThrottle(AIN_typeDef throttle1, AIN_typeDef throttle2)
{
    float t1 = 0; // Throttle 1 value
    float t2 = 0; // Throttle 2 value
    float relError = 0; // The relative error between them
    float average = 1;
    /* Scale the raw data before implausability test */
    t1 = scaleInput(throttle1);
    t2 = scaleInput(throttle2);
    
    /* Calculate relative error */
    if(t1 == t2)
    {
        // Do nothing
    }else
    {
        average = (t1+t2)/2;
        relError = fabs((t1-t2)/average);
    }
    /* Check error if error is to large*/
    if(relError >= TRHOTTLE_IMPLAUSABILITY_THRESHOLD)
    {
         BSP_CAN_TxID(ID_TX_ERROR_THROTTLE_IMPLASUABILITY);
        //set flag fatal error!
        //Go in fatal error mode...
        return -1;
    }else
    {
        return (uint16_t)((t1+t2)/2);
    }
}

uint16_t BrakeFront(uint16_t value, uint16_t calMin, uint16_t calMax)
{
    return 0;
}
static float scaleInput(AIN_typeDef input)
{
    float scalingFactor = 1;
    float output = 0;
    if(input.calMax == 0 && input.calMin == 0) //Sensor don't need scaling.
    {
        return 0;
    }
    else if (input.calMax <= input.calMin) // Illegal case.
    {
        return -1;
    }else
    {
    scalingFactor = 4095.0f/((float)(input.calMax-input.calMin));
    output = ((float)(input.value - input.calMin)) * scalingFactor;
    return output;
    }
}